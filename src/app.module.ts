import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { KafkaProducerService } from './kafka-producer/kafka-producer.service';
import { MerchantModule } from './merchant/merchant.module';

@Module({
  imports: [MerchantModule],
  controllers: [AppController],
  providers: [AppService, KafkaProducerService],
})
export class AppModule {}
