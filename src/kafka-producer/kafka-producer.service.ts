import {
  Injectable,
  OnApplicationShutdown,
  OnModuleInit,
} from '@nestjs/common';
import { Kafka, logLevel, Producer, ProducerRecord } from 'kafkajs';

@Injectable()
export class KafkaProducerService
  implements OnModuleInit, OnApplicationShutdown
{
  private readonly kafka: Kafka = new Kafka({
    brokers: ['localhost:9092'],
    clientId: 'nestjs-kafka',
    logLevel: logLevel.INFO,
  });
  private readonly producer: Producer = this.kafka.producer();

  async onModuleInit() {
    await this.producer.connect();
  }

  produce(topic: string, key: string, message: string) {
    //prepare payload
    const producerRecord: ProducerRecord = {
      topic: topic,
      messages: [
        {
          key: key,
          value: message,
        },
      ],
    };

    //Send Records to Kafka asynchronously
    this.producer.send(producerRecord).then((r) => console.log(r));
  }

  async onApplicationShutdown() {
    await this.producer.disconnect();
  }
}
