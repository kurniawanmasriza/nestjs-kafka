import {
  Injectable,
  OnApplicationShutdown,
  OnModuleInit,
} from '@nestjs/common';
import {
  Kafka,
  KafkaConfig,
  logLevel,
  Producer,
  ProducerRecord,
} from 'kafkajs';

const kafkaConfig: KafkaConfig = {
  brokers: ['10.1.76.85:9093', '10.1.76.86:9093', '10.1.76.88:9093'],
  clientId: 'nestjs-kafka',
  sasl: {
    mechanism: 'scram-sha-256',
    username: 'admin',
    password: 'P@ssw0rd',
  },
  logLevel: logLevel.INFO,
};

@Injectable()
export class KafkaConfluentProducerService
  implements OnModuleInit, OnApplicationShutdown
{
  private readonly kafka: Kafka = new Kafka(kafkaConfig);
  private readonly producer: Producer = this.kafka.producer();

  async onModuleInit() {
    await this.producer.connect();
  }

  produce(topic: string, key: string, message: string) {
    //prepare payload
    const producerRecord: ProducerRecord = {
      topic: topic,
      messages: [
        {
          key: key,
          value: message,
        },
      ],
    };

    //Send Records to Kafka asynchronously
    this.producer.send(producerRecord).then((r) => console.log(r));
  }

  async onApplicationShutdown() {
    await this.producer.disconnect();
  }
}
