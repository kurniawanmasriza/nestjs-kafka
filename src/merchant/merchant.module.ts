import { Module } from '@nestjs/common';
import { MerchantService } from './merchant.service';
import { MerchantController } from './merchant.controller';
import { KafkaProducerService } from '../kafka-producer/kafka-producer.service';
import { KafkaConfluentProducerService } from '../kafka-producer/kafka-confluent-producer.service';

@Module({
  controllers: [MerchantController],
  providers: [
    MerchantService,
    KafkaProducerService,
    KafkaConfluentProducerService,
  ],
})
export class MerchantModule {}
