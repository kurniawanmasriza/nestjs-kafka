import { Body, Controller, Post } from '@nestjs/common';
import { MerchantService } from './merchant.service';
import { Merchant } from './entities/merchant.entity';

@Controller('merchant')
export class MerchantController {
  constructor(private readonly merchantService: MerchantService) {}

  @Post()
  createKey(@Body() merchant: Merchant) {
    this.merchantService.produceKafkaConfluent(merchant);
    return 'ok';
  }
}
