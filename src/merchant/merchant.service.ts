import { Injectable } from '@nestjs/common';
import { KafkaProducerService } from '../kafka-producer/kafka-producer.service';
import { Merchant } from './entities/merchant.entity';
import { KafkaConfluentProducerService } from '../kafka-producer/kafka-confluent-producer.service';

@Injectable()
export class MerchantService {
  constructor(
    private readonly kafkaProducerService: KafkaProducerService,
    private readonly kafkaConfluentProducerService: KafkaConfluentProducerService,
  ) {}

  produceKafka(merchant: Merchant) {
    const message = JSON.stringify(merchant);
    this.kafkaProducerService.produce('ID.TEST', merchant.mid, message);
  }

  produceKafkaConfluent(merchant: Merchant) {
    const message = JSON.stringify(merchant);
    this.kafkaConfluentProducerService.produce(
      'ID.PAYMENT.TEST.EVENT',
      merchant.mid,
      message,
    );
  }
}
